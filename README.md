# Yugabyte test

[Source](https://blog.yugabyte.com/using-envoy-proxys-postgresql-tcp-filters-to-collect-yugabyte-sql-statistics/)

```bash
docker-compose pull
docker-compose up --build -d
docker-compose ps
```

Connect to Yuga container:

```
docker run --rm -it --network envoymesh yugabytedb/yugabyte /home/yugabyte/bin/ysqlsh "sslmode=disable" -h envoy -p 1999
```
Connect through proxy:

```bash
psql -h localhost -p 5444 -Upostgres yugabyte
```



Envoy stats exposed:

```
http://localhost:8001/stats
```

[Documentation Architecture](https://github.com/yugabyte/yugabyte-db)


